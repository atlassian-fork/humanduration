package com.atlassian.humanduration.spring;

import com.atlassian.humanduration.HumanDuration;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;

import java.time.Duration;

@ConfigurationPropertiesBinding
public class HumanDurationToDurationConverter implements Converter<String, Duration> {

    @Override
    public Duration convert(String text) {
        return HumanDuration.stringToDuration(text);
    }
}
