package com.atlassian.humanduration;


import org.junit.Test;

import java.time.Duration;

import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class HumanDurationTest {

    @Test(expected = MalformedHumanDurationException.class)
    public void testEmptyInputRaisesException() {
        HumanDuration.stringToDuration("");
    }

    @Test(expected = MalformedHumanDurationException.class)
    public void testBlankInputRaisesException() {
        HumanDuration.stringToDuration("    ");
    }

    @Test(expected = MalformedHumanDurationException.class)
    public void testNegativeNumberRaisesException() {
        HumanDuration.stringToDuration("-1");
    }

    @Test(expected = MalformedHumanDurationException.class)
    public void testOddPartsNumberRaisesException() {
        HumanDuration.stringToDuration("1s2");
    }

    @Test(expected = MalformedHumanDurationException.class)
    public void testSingleNumberRaisesException() {
        HumanDuration.stringToDuration("239");
    }

    @Test(expected = MalformedHumanDurationException.class)
    public void testSingleNonNumberRaisesException() {
        HumanDuration.stringToDuration("test");
    }

    @Test(expected = MalformedHumanDurationException.class)
    public void testSingleNonAlphanumericRaisesException() {
        HumanDuration.stringToDuration(".");
    }

    @Test(expected = MalformedHumanDurationException.class)
    public void testUnknownTimeUnitRaisesException() {
        HumanDuration.stringToDuration("1k");
    }

    @Test(expected = MalformedHumanDurationException.class)
    public void testWrongOrderRaisesException() {
        HumanDuration.stringToDuration("s1");
    }

    @Test
    public void testLeadingZerosAreIgnored() {
        assertThat(HumanDuration.stringToDuration("001day"), equalTo(Duration.ofHours(24)));
    }

    @Test
    public void testWhitespacesAreIgnored() {
        assertThat(HumanDuration.stringToDuration("   1 \t  day   "), equalTo(Duration.ofDays(1)));
        assertThat(HumanDuration.stringToDuration("\t1\tday1hour\t"), equalTo(Duration.ofHours(25)));
        assertThat(HumanDuration.stringToDuration(" 0s 0ms "), equalTo(Duration.ZERO));
    }

    @Test
    public void testParseDuration() {
        assertThat(HumanDuration.stringToDuration("1 hour 2 minutes 3 seconds"), equalTo(Duration.ofSeconds(3723)));
        assertThat(HumanDuration.stringToDuration("3h 2m 1s"), equalTo(Duration.ofSeconds(10921)));
        assertThat(HumanDuration.stringToDuration("60s"), equalTo(Duration.ofMinutes(1)));
        assertThat(HumanDuration.stringToDuration("1 minute 5 seconds"), equalTo(Duration.ofSeconds(65)));
        assertThat(HumanDuration.stringToDuration("1m5s"), equalTo(Duration.ofSeconds(65)));
        assertThat(HumanDuration.stringToDuration("239s 405ms"), equalTo(Duration.ofMillis(239405)));
        assertThat(HumanDuration.stringToDuration("1d 2d"), equalTo(Duration.ofDays(3)));
        assertThat(HumanDuration.stringToDuration("1ms 1h 4w"), equalTo(Duration.ofDays(7).multipliedBy(4).plus(1, HOURS).plus(1, MILLIS)));
    }

    @Test
    public void testParseZero() {
        assertThat(HumanDuration.stringToDuration("0"), equalTo(Duration.ZERO));
        assertThat(HumanDuration.stringToDuration("000"), equalTo(Duration.ZERO));
        assertThat(HumanDuration.stringToDuration("0s 0ms"), equalTo(Duration.ZERO));
    }

    @Test
    public void testWeekCalculatedWithEstimatedDuration() {
        assertThat(HumanDuration.stringToDuration("1w"), equalTo(Duration.ofDays(7)));
    }
}
